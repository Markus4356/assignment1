﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Assignment1.Enumerators;

namespace Assignment1.Items
{
    public class Weapon : Item
    {
        //Constructor
        public Weapon(string name, int levelRequired, Slot itemslot, WeaponTypes type)
            : base(name, levelRequired, itemslot)
        {
            Type = type;
        }

        public WeaponTypes Type { get; set; }

        /// <summary>
        /// Base damage for the different weapons
        /// </summary>
        /// Returns the base damage of the weapon (int)
        public int BaseDamage()
        {
            int damage = 0;

            switch (Type)
            {
                case WeaponTypes.Axe:
                    damage = 7;
                    break;
                case WeaponTypes.Bow:
                    damage = 5;
                    break;
                case WeaponTypes.Dagger:
                    damage = 4;
                    break;
                case WeaponTypes.Hammer:
                    damage = 6;
                    break;
                case WeaponTypes.Staff:
                    damage = 7;
                    break;
                case WeaponTypes.Sword:
                    damage = 8;
                    break;
                case WeaponTypes.Wand:
                    damage = 4;
                    break;
            }
            return damage;
        }
        /// <summary>
        /// Attacks per second for the different weapons
        /// </summary>
        /// <returns></returns>
        /// Returns attack per seconds for the specified weapon (int)
        public int AttacksPerSecond()
        {
            int speed = 0;

            switch (Type)
            {
                case WeaponTypes.Axe:
                    speed = 5;
                    break;
                case WeaponTypes.Bow:
                    speed = 7;
                    break;
                case WeaponTypes.Dagger:
                    speed = 7;
                    break;
                case WeaponTypes.Hammer:
                    speed = 3;
                    break;
                case WeaponTypes.Staff:
                    speed = 3;
                    break;
                case WeaponTypes.Sword:
                    speed = 3;
                    break;
                case WeaponTypes.Wand:
                    speed = 7;
                    break;
            }
            return speed;
        }
        /// <summary>
        /// Weapon DPS: Base Damage * Attacks per second
        /// </summary>
        /// <returns></returns>
        /// Returns DPS (int)
        public int DPS()
        {
            return (BaseDamage() * AttacksPerSecond());
        }
    }
}
