﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Assignment1.Enumerators;

namespace Assignment1.Items
{
    public class Armor : Item
    {
        //Constructor
        public Armor(string name, int levelRequired, Slot ItemSlot, ArmorTypes type, PrimaryAttributes attributes)
            : base(name, levelRequired, ItemSlot)
        {
            Type = type;
            Attributes = attributes;
        }

        public ArmorTypes Type { get; set; }
        public PrimaryAttributes Attributes { get; set; }
    }
}
