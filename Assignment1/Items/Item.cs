﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Assignment1.Enumerators;

namespace Assignment1.Items
{
    public abstract class Item
    {
        //Constructor
        public Item(string name, int levelRequired, Slot itemSlot)
        {
            Name = name;
            LevelRequired = levelRequired;
            ItemSlot = itemSlot;
        }

        public string Name { get; set; }
        public int LevelRequired { get; set; }
        public Slot ItemSlot { get; set; }

    }
}
