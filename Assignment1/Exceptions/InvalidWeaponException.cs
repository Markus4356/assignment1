﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assignment1.Exceptions
{
    public class InvalidWeaponException : Exception
    {
        //Custom exception for invalid weapon
        public InvalidWeaponException()
        {
        }
    }
}
