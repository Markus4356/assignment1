﻿using Assignment1.Enumerators;
using Assignment1.Exceptions;
using Assignment1.Items;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection.Emit;
using System.Text;
using System.Threading.Tasks;

namespace Assignment1.Classes
{
    public class Ranger : Character
    {
        //Constructor
        public Ranger()
        {
            BasePrimaryAttributes = basePrimaryAttributes;
            TotalPrimaryAttributes = BasePrimaryAttributes;

            PrimarySkill = TotalPrimaryAttributes.Dexterity;
            LevelAttributes = levelAttributes;

            Class = "Ranger";
        }
        //Constructor with name as input
        public Ranger(string name) : base(name)
        {
            Name = name;
            BasePrimaryAttributes = basePrimaryAttributes;
            TotalPrimaryAttributes = BasePrimaryAttributes;
            PrimarySkill = TotalPrimaryAttributes.Dexterity;
            Class = "Ranger";
        }
        //Base Primary Attributes. The ranger starts with these attributes
        PrimaryAttributes basePrimaryAttributes = new PrimaryAttributes()
        {
            Strength = 1,
            Dexterity = 7,
            Intelligence = 1
        };
        //Level Attributes. The ranger gain these attributes for each level
        PrimaryAttributes levelAttributes = new PrimaryAttributes
        {
            Strength = 1,
            Dexterity = 5,
            Intelligence = 1
        };

        //Updates the primary skill for the ranger (Dexterity).
        public override void UpdatePrimarySkill()
        {
            PrimarySkill = TotalPrimaryAttributes.Dexterity;
        }
    }
}