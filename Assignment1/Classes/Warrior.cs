﻿using Assignment1.Enumerators;
using Assignment1.Exceptions;
using Assignment1.Items;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection.Emit;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace Assignment1.Classes
{
    public class Warrior : Character
    {
        //Constructor
        public Warrior()
        {
            BasePrimaryAttributes = basePrimaryAttributes;
            TotalPrimaryAttributes = BasePrimaryAttributes;

            PrimarySkill = TotalPrimaryAttributes.Strength;
            LevelAttributes = levelAttributes;

            Class = "Warrior";
        }
        //Constructor with name as input
        public Warrior(string name) : base(name)
        {
            Name = name;

            BasePrimaryAttributes = basePrimaryAttributes;
            TotalPrimaryAttributes = BasePrimaryAttributes;

            LevelAttributes = levelAttributes;
            PrimarySkill = TotalPrimaryAttributes.Strength;

            Class = "Warrior";
        }
        //Base Primary Attributes. The warrior starts with these attributes
        PrimaryAttributes basePrimaryAttributes = new PrimaryAttributes()
        {
            Strength = 5,
            Dexterity = 2,
            Intelligence = 1
        };
        //Level Attributes. The warrior gain these attributes for each level
        PrimaryAttributes levelAttributes = new PrimaryAttributes
        {
            Strength = 3,
            Dexterity = 2,
            Intelligence = 1
        };

        //Updates the primary skill for the warrior (Strength).
        public override void UpdatePrimarySkill()
        {
            PrimarySkill = TotalPrimaryAttributes.Strength;
        }
    }
}
