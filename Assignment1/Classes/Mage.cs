﻿using Assignment1.Classes;
using Assignment1.Enumerators;
using Assignment1.Exceptions;
using Assignment1.Items;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection.Metadata.Ecma335;
using System.Text;
using System.Threading.Tasks;

namespace Assignment1.Classes
{
    public class Mage : Character
    {
        //Constructor
        public Mage()
        {
            BasePrimaryAttributes = basePrimaryAttributes;
            TotalPrimaryAttributes = BasePrimaryAttributes;

            PrimarySkill = TotalPrimaryAttributes.Intelligence;
            LevelAttributes = levelAttributes;

            Class = "Mage";
        }
        //Constructor with name as input
        public Mage(string name) : base(name)
        {
            Name = name;

            BasePrimaryAttributes=basePrimaryAttributes;
            TotalPrimaryAttributes = BasePrimaryAttributes;

            PrimarySkill = TotalPrimaryAttributes.Intelligence;
            LevelAttributes = levelAttributes;

            Class = "Mage";
        }
        //Base Primary Attributes. The mage starts with these attributes
        PrimaryAttributes basePrimaryAttributes = new PrimaryAttributes()
        {
            Strength = 1,
            Dexterity = 1,
            Intelligence = 8
        };
        //Level Attributes. The mage gain these attributes for each level
        PrimaryAttributes levelAttributes = new PrimaryAttributes
        {
            Strength = 1,
            Dexterity = 1,
            Intelligence = 5
        };

        //Updates the primary skill for the mage (Intelligence)
        public override void UpdatePrimarySkill()
        {
            PrimarySkill = TotalPrimaryAttributes.Intelligence;
        }
    }
}
