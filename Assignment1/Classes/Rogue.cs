﻿using Assignment1.Enumerators;
using Assignment1.Exceptions;
using Assignment1.Items;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection.Emit;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace Assignment1.Classes
{
    public class Rogue : Character
    {
        //Constructor
        public Rogue()
        {
            BasePrimaryAttributes = basePrimaryAttributes;
            TotalPrimaryAttributes = BasePrimaryAttributes;

            PrimarySkill = TotalPrimaryAttributes.Dexterity;
            LevelAttributes = levelAttributes;

            Class = "Rogue";
        }
        //Constructor with name as input
        public Rogue(string name) : base(name)
        {
            Name = name;

            BasePrimaryAttributes = basePrimaryAttributes;
            TotalPrimaryAttributes = BasePrimaryAttributes;

            PrimarySkill = TotalPrimaryAttributes.Dexterity;
            LevelAttributes = levelAttributes;

            Class = "Rogue";
        }

        //Base Primary Attributes. The rogue starts with these attributes
        PrimaryAttributes basePrimaryAttributes = new PrimaryAttributes()
        {
            Strength = 2,
            Dexterity = 6,
            Intelligence = 1
        };
        //Level Attributes. The rogue gain these attributes for each level
        PrimaryAttributes levelAttributes = new PrimaryAttributes
        {
            Strength = 1,
            Dexterity = 4,
            Intelligence = 1
        };

        //Updates the primary skill for the rogue (Dexterity).
        public override void UpdatePrimarySkill()
        {
            PrimarySkill = TotalPrimaryAttributes.Dexterity;
        }
    }
}