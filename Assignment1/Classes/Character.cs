﻿using Assignment1.Enumerators;
using Assignment1.Exceptions;
using Assignment1.Items;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection.Emit;
using System.Text;
using System.Threading.Tasks;
using static System.Net.Mime.MediaTypeNames;

namespace Assignment1.Classes
{
    public abstract class Character
    {
        //Constructor
        public Character()
        {
            Inventory1 = new Dictionary<Slot, Item>();
        }

        //Constructor with name as input
        public Character(string name)
        {
            Name = name;

            Inventory1 = new Dictionary<Slot, Item>();
        }

        //Properties that all characters have
        public string Name { get; set; }
        public int Level { get; set; } = 1;

        public PrimaryAttributes BasePrimaryAttributes { get; set; }
        public PrimaryAttributes TotalPrimaryAttributes { get; set; }
        public PrimaryAttributes LevelAttributes { get; set; }

        //I am using this to calculate damage in this class, since
        //different heroes have a different attribute that influence damage
        public int PrimarySkill { get; set; }

        //I need to differentiate between the classes, so I can decide if
        //weapon/armor is legal to equip
        public string Class { get; set; }
        
        //dictionary to store items in different slots
        public Dictionary<Slot, Item> Inventory1 { get; set; }

        //Abstract method. Implemented in each character class
        public abstract void UpdatePrimarySkill();

        //Implemented methods

        /// <summary>
        /// Level Up. Level gets raised by one and the stats for the
        /// level added. It also runs the method UpdateAttributes() and
        /// StatsDisplay()
        /// </summary>
        public virtual void LevelUp()
        {
            Level += 1;


            BasePrimaryAttributes += LevelAttributes;
            UpdateAttributes();

            Console.WriteLine("Level Up!");
            StatsDisplay();
        }

        //Checks if the character level is high enough to equip a item.
        //Return: true -> can equip
        //        false -> can not equip
        public virtual bool ItemLevelCheck(Item item)
        {
            if (item.LevelRequired > Level)
                return false;
            else
                return true;
        }

        //Add a item to the inventory (dictionary)
        public virtual void Inventory(Item item)
        {
            Inventory1[item.ItemSlot] = item;
        }

        //Checks if the character have a weapon
        //If it does: calculates with DPS from weapon.
        //if it does not: set DPS to 1.
        //Return: damage calculated (double)
        public virtual double CharacterDamage()
        {
            double DPS = 0.0;
            double damage = 0.0;

            if (!Inventory1.ContainsKey(Slot.Weapon))
            {
                DPS = 1.0;
            }
            else
            {
                Weapon weapon = (Weapon)Inventory1[Slot.Weapon];
                DPS = weapon.DPS();
            }
            double skill = PrimarySkill;
            damage = (DPS * (1.0 + skill / 100.0));
            return damage;
        }

        //Updates TotalPrimaryAttributes:
        //Base Attributes + armor attributes
        //It also have to use UpdatePrimarySkill() method,
        //since the int it contains may have changed.
        public void UpdateAttributes()
        {
            TotalPrimaryAttributes = BasePrimaryAttributes;

            foreach (KeyValuePair<Slot, Item> item in Inventory1)
            {

                if (!(item.Key == Slot.Weapon))
                {
                    Armor armor = (Armor)item.Value;

                    TotalPrimaryAttributes += armor.Attributes;
                }
            }
            UpdatePrimarySkill();
        }

        //Writes a stat display to the console,
        //using stringbuilder.
        public virtual void StatsDisplay()
        {
            StringBuilder display = new StringBuilder();

            display.Append($"____________________\n\nName: {Name}\nLevel: {Level}\n");
            display.Append($"____________________\n\nBase | Total" +
                $"    \n  Strength\n{BasePrimaryAttributes.Strength}    |    {TotalPrimaryAttributes.Strength}" +
                $"\n  Dexterity\n{BasePrimaryAttributes.Dexterity}    |    {TotalPrimaryAttributes.Dexterity}" +
                $"\n  Intelligence\n{BasePrimaryAttributes.Intelligence}    |    {TotalPrimaryAttributes.Intelligence}");
            display.Append($"\n____________________\n\nDamage: {CharacterDamage()}\n____________________");

            Console.WriteLine(display);
        }

        //Writes a inventory display to the console,
        //using stringbuilder.
        public virtual void InventoryDisplay()
        {
            StringBuilder display = new StringBuilder();

            
            if (Inventory1.ContainsKey(Slot.Weapon))
            {
                Weapon weapon = (Weapon)Inventory1[Slot.Weapon];
                display.AppendLine($"____________________\r\n\r\nWEAPON\r\n{weapon.Name}");

            }
            else
            {
                display.AppendLine("____________________\r\n\r\nWEAPON\r\nNo weapon");
            }

            if (Inventory1.ContainsKey(Slot.Head))
            {
                Item head = (Armor)Inventory1[Slot.Head];
                display.AppendLine($"____________________\r\n\r\nHEAD\r\n{head.Name}");

            }
            else
            {
                display.AppendLine($"____________________\r\n\r\nHEAD\r\nNo head armor");
            }

            if (Inventory1.ContainsKey(Slot.Body))
            {
                Item body = (Armor)Inventory1[Slot.Body];
                display.AppendLine($"____________________\r\n\r\nHEAD\r\n{body.Name}");

            }
            else
            {
                display.AppendLine($"____________________\r\n\r\nHEAD\r\nNo body armor");
            }

            if (Inventory1.ContainsKey(Slot.Legs))
            {
                Item legs = (Armor)Inventory1[Slot.Legs];
                display.AppendLine($"____________________\r\n\r\nHEAD\r\n{legs.Name}");

            }
            else
            {
                display.AppendLine($"____________________\r\n\r\nLEGS\r\nNo leg armor\r\n____________________");
            }

            Console.WriteLine(display);
        }


        /// <summary>
        /// checks wether the character can equip a weapon
        /// if it can, it will be added to the inventory
        /// </summary>
        /// <param name="weapon"></param>
        /// the weapon to be equipped
        /// <returns></returns> A string
        /// <exception cref="InvalidWeaponException"></exception>
        /// if the character level is to low, or wrong weapon type
        /// the exception InvalidWeaponException is thrown
        public virtual string EquipWeapon(Weapon weapon)
        {
            string success = "New weapon equipped!";

            if (!ItemLevelCheck(weapon))
            {
                throw new InvalidWeaponException();
            }

            switch (Class)
            {
                case "Mage":
                    if (weapon.Type != WeaponTypes.Staff && weapon.Type != WeaponTypes.Wand)
                    {
                        throw new InvalidWeaponException();
                    }
                    Inventory(weapon);
                    return success;
                case "Ranger":
                    if (weapon.Type != WeaponTypes.Bow)
                    {
                        throw new InvalidWeaponException();
                    }
                    Inventory(weapon);
                    return success;
                case "Rogue":
                    if (weapon.Type != WeaponTypes.Dagger && weapon.Type != WeaponTypes.Sword)
                    {
                        throw new InvalidWeaponException();
                    }
                    Inventory(weapon);
                    return success;
                case "Warrior":
                    if (weapon.Type != WeaponTypes.Axe && weapon.Type != WeaponTypes.Hammer && weapon.Type != WeaponTypes.Sword)
                    {
                        throw new InvalidWeaponException();
                    }
                    Inventory(weapon);
                    return success;

            }
            return "Unexpected Class";
        }

        /// <summary>
        /// checks if the character can equip an armor.
        /// if it can, it will be added to the inventory
        /// </summary>
        /// <param name="armor"></param>
        /// the armor to be equipped
        /// <returns></returns>
        /// A string
        /// <exception cref="InvalidArmorException"></exception>
        /// if the character level is to low, or wrong weapon type
        /// InvalidArmorException is thrown
        public virtual string EquipArmor(Armor armor)
        {
            string success = "New armor equipped!";

            if (!ItemLevelCheck(armor))
            {
                throw new InvalidArmorException();
            }

            switch (Class)
            {
                case "Mage":
                    if (armor.Type != ArmorTypes.Cloth)
                    {
                        throw new InvalidArmorException();
                    }
                    Inventory(armor);
                    UpdateAttributes();

                    return success;
                case "Ranger":
                    if (armor.Type != ArmorTypes.Leather && armor.Type != ArmorTypes.Mail)
                    {
                        throw new InvalidArmorException();
                    }
                    Inventory(armor);
                    UpdateAttributes();

                    return success;
                case "Rogue":
                    if (armor.Type != ArmorTypes.Leather && armor.Type != ArmorTypes.Mail)
                    {
                        throw new InvalidArmorException();
                    }
                    Inventory(armor);
                    UpdateAttributes();

                    return success;
                case "Warrior":
                    if (armor.Type != ArmorTypes.Mail && armor.Type != ArmorTypes.Plate)
                    {
                        throw new InvalidArmorException();
                    }
                    Inventory(armor);
                    UpdateAttributes();

                    return success;
            }
            return "Unexpected class";
        }
    }
}
