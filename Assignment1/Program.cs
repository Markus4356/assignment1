﻿using Assignment1;
using Assignment1.Classes;
using Assignment1.Enumerators;
using Assignment1.Items;
using System.Runtime.InteropServices;
using static System.Net.Mime.MediaTypeNames;

//Only added items and weapons for mage here
static Character ChooseHero()
{
    Character hero;

    Mage testhero = new Mage("testhero");
    hero = testhero;

    Console.WriteLine("Choose character: ");
    Console.WriteLine("\n a. Mage \n b. Ranger \n c. Rogue \n d. Warrior");
    string userinput = Console.ReadLine();

    while(userinput!="a" && userinput != "b" && userinput != "c" && userinput != "d")
    {
        Console.WriteLine("Choose character: ");
        Console.WriteLine("\n a. Mage \n b. Ranger \n c. Rogue \n d. Warrior");
        userinput = Console.ReadLine();
    }

    if (userinput == "a")
    {
        Mage mage1 = new Mage("Harry");
        hero = mage1;
    }
    else if (userinput == "b")
    {
        Ranger ranger1 = new Ranger("Legolas");
        hero = ranger1;
    }
    else if (userinput == "c")
    {
        Rogue rogue1 = new Rogue("Sam");
        hero = rogue1;
    }
    else if (userinput == "d")
    {
        Warrior warrior1 = new Warrior("Tor");
        hero = warrior1;
    }
    return hero;
}

Character hero;
hero = ChooseHero();
hero.StatsDisplay();

Console.WriteLine("Take a fight? y/n");
string fight = Console.ReadLine();

if (fight == "y")
{
    hero.LevelUp();
}

Weapon weapon = new Weapon("Wand", 2, Slot.Weapon, WeaponTypes.Wand);
Armor armor1 = new Armor("Chest Armor", 2, Slot.Body, ArmorTypes.Cloth,
    new PrimaryAttributes { Strength = 1, Dexterity = 4, Intelligence = 4 });
Armor armor2 = new Armor("Head Armor", 1, Slot.Head, ArmorTypes.Cloth,
    new PrimaryAttributes {Strength= 3, Dexterity=1, Intelligence=2});

Console.WriteLine("You found weapon and armor. Would you like to equip? y/n");
string equip = Console.ReadLine();

if(equip== "y")
{
    hero.EquipWeapon(weapon);
    hero.EquipArmor(armor1);
    hero.EquipArmor(armor2);
}

hero.StatsDisplay();
hero.InventoryDisplay();