﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assignment1.Enumerators
{
    /// <summary>
    /// Slot is an enumerator for the different slots a
    /// character can equip something.
    /// </summary>
    public enum Slot
    {
        Head,
        Body,
        Legs,
        Weapon
    }
}
