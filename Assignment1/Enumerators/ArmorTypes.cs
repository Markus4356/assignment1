﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assignment1.Enumerators
{
    /// <summary>
    /// ArmorTypes is an enumerator for the different types of armor
    /// the characters can equip
    /// </summary>
    public enum ArmorTypes
    {
        Cloth,
        Leather,
        Mail,
        Plate
    }
}
