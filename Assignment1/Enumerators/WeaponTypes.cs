﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assignment1.Enumerators
{
    /// <summary>
    /// WeaponTypes is an enumerator for the different
    /// weapons a character can equip
    /// </summary>
    public enum WeaponTypes
    {
        Axe,
        Bow,
        Dagger,
        Hammer,
        Staff,
        Sword,
        Wand
    }
}
