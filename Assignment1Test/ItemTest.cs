﻿using Assignment1;
using Assignment1.Classes;
using Assignment1.Enumerators;
using Assignment1.Exceptions;
using Assignment1.Items;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assignment1Test
{
    public class ItemTest
    {
        [Fact]
        public void EquipWeapon_EquipHighLevelWeapon_ShouldThrowInvalidWeaponException()
        {
            //Arrange
            Warrior hero = new Warrior();
            Weapon testAxe = new Weapon("Common axe", 2, Slot.Weapon, WeaponTypes.Axe);

            //Act & Assert
            Assert.Throws<InvalidWeaponException>(() => hero.EquipWeapon(testAxe));
        }

        [Fact]
        public void EquipArmor_EquipHighLevelArmor_ShouldThrowInvalidArmorException()
        {
            //Arrange
            Warrior hero = new Warrior();
            Armor testPlateBody = new Armor("Common plate body armor", 2,
                Slot.Body, ArmorTypes.Plate, new PrimaryAttributes() { Strength = 1 });

            //Act & Assert
            Assert.Throws<InvalidArmorException>(() => hero.EquipArmor(testPlateBody));
        }

        [Fact]
        public void EquipWeapon_EquipWrongWeaponType_ShouldThrowInvalidWeaponException()
        {
            //Arrange
            Warrior hero = new Warrior();
            Weapon testBow = new Weapon("Common Bow", 1, Slot.Weapon, WeaponTypes.Bow);

            //Act & Assert
            Assert.Throws<InvalidWeaponException>(() => hero.EquipWeapon(testBow));
        }

        [Fact]
        public void EquipArmor_EquipWrongArmorType_ShouldThrowInvalidArmorException()
        {
            //Arrange
            Warrior hero = new Warrior();
            Armor testClothHead = new Armor("Common cloth head armor", 1,
                Slot.Head, ArmorTypes.Cloth, new PrimaryAttributes { Intelligence = 5 });

            //Act & Assert
            Assert.Throws<InvalidArmorException>(() => hero.EquipArmor(testClothHead));
        }

        [Fact]
        public void EquipWeapon_EquipValidWeapon_ShouldReturnMessage()
        {
            //Arrange
            Warrior hero = new Warrior();
            Weapon testAxe = new Weapon("Common axe", 1, Slot.Weapon, WeaponTypes.Axe);
            
            string expected = "New weapon equipped!";

            //Act
            string actual = hero.EquipWeapon(testAxe);

            //Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void EquipArmor_EquipValidArmor_ShouldReturnMessage()
        {
            //Arrange
            Warrior hero = new Warrior();
            Armor testPlateBody = new Armor("Common plate body armor", 1,
                Slot.Body, ArmorTypes.Plate, new PrimaryAttributes() { Strength = 1 });

            string expected = "New armor equipped!";

            //Act
            string actual = hero.EquipArmor(testPlateBody);

            //Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void CharacterDamage_DamageNoWeapon_ReturnDamage()
        {
            //Arrange
            Warrior hero = new Warrior();
            double expectedDamage = 1 * (1 + (5.0/100.0));
            //Act
            double actualDamage = hero.CharacterDamage();
            //Assert
            Assert.Equal(expectedDamage, actualDamage);
        }

        [Fact]
        public void CharacterDamage_DamageWithWeapon_ReturnDamage()
        {
            //Arrange
            Warrior hero = new Warrior();
            Weapon testAxe = new Weapon("Common axe", 1, Slot.Weapon, WeaponTypes.Axe);

            double expectedDamage = (7*5)* (1 + (5.0 / 100.0));

            //Act
            hero.EquipWeapon(testAxe);
            double actualDamage = hero.CharacterDamage();

            //Assert
            Assert.Equal(expectedDamage, actualDamage);
        }

        [Fact]
        public void CharacterDamage_DamageWithWeaponAndArmor_ReturnDamage()
        {
            //Arrange
            Warrior hero = new Warrior();
            Weapon testAxe = new Weapon("Common axe", 1, Slot.Weapon, WeaponTypes.Axe);
            Armor testPlateBody = new Armor("Common plate body armor", 1,
                Slot.Body, ArmorTypes.Plate, new PrimaryAttributes() { Strength = 1 });

            double expectedDamage = (7 * 5) * (1 + (6.0 / 100.0));

            //Act
            hero.EquipWeapon(testAxe);
            hero.EquipArmor(testPlateBody);
            double actualDamage = hero.CharacterDamage();

            //Assert
            Assert.Equal(expectedDamage, actualDamage);
        }
    }
}
