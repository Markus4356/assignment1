using Assignment1;
using Assignment1.Classes;

namespace Assignment1Test
{
    public class CharacterTest
    {

        //A character is level 1 when created
        [Fact]
        public void Character_Level1WhenCreated_ShouldReturnTrue()
        {
            //Arrange
            Mage hero = new Mage();
            //Ranger hero = new Ranger(); //Rogue hero = new Rogue(); //Warrior hero = new Warrior();

            int expectedLevel = 1;
            //Act
            int actualLevel = hero.Level;
            //Assert
            Assert.Equal(expectedLevel, actualLevel);
        }

        //When a character gains a level, it should be level 2
        [Fact]
        public void LevelUp_GainALevel_ShouldBeLevel2()
        {
            //Arrange
            Mage hero = new Mage();
            int expectedLevel = 2;
            //Act
            hero.LevelUp();
            int actualLevel = hero.Level;
            //Assert
            Assert.Equal(expectedLevel, actualLevel);
        }

        //Each character class is created with the proper default attributes
        [Fact]
        public void Mage_CreatedWithProperAttributes_ShouldUpdateMagePrimaryAttributes()
        {
            //Arrange
            Mage hero = new Mage();
            PrimaryAttributes expected = new PrimaryAttributes{Strength=1,Dexterity=1,Intelligence=8};
            //Act
            PrimaryAttributes actual = hero.BasePrimaryAttributes;
            //Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void Ranger_CreatedWithProperAttributes_ShouldUpdateRangerPrimaryAttributes()
        {
            //Arrange
            Ranger hero = new Ranger();
            PrimaryAttributes expected = new PrimaryAttributes { Strength = 1, Dexterity = 7, Intelligence = 1 };
            //Act
            PrimaryAttributes actual = hero.BasePrimaryAttributes;
            //Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void Rogue_CreatedWithProperAttributes_ShouldUpdateRoguePrimaryAttributes()
        {
            //Arrange
            Rogue hero = new Rogue();
            PrimaryAttributes expected = new PrimaryAttributes { Strength = 2, Dexterity = 6, Intelligence = 1 };
            //Act
            PrimaryAttributes actual = hero.BasePrimaryAttributes;
            //Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void Warrior_CreatedWithProperAttributes_ShouldUpdateWarriorPrimaryAttributes()
        {
            //Arrange
            Warrior hero = new Warrior();
            PrimaryAttributes expected = new PrimaryAttributes { Strength = 5, Dexterity = 2, Intelligence = 1 };
            //Act
            PrimaryAttributes actual = hero.BasePrimaryAttributes;
            //Assert
            Assert.Equal(expected, actual);
        }

        //Each character class has their attributes increased when leveling up
        [Fact]
        public void MageLevelUp_UpdateAttribute_AttributesPlusLevelAttributes()
        {
            //Arrange
            Mage hero = new Mage();
            PrimaryAttributes expected = new PrimaryAttributes
            {
                Strength = 2,
                Dexterity = 2,
                Intelligence = 13
            };

            //Act
            hero.LevelUp();
            PrimaryAttributes actual = hero.BasePrimaryAttributes;

            //Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void RangerLevelUp_UpdateAttribute_AttributesPlusLevelAttributes()
        {
            //Arrange
            Ranger hero = new Ranger();
            PrimaryAttributes expected = new PrimaryAttributes
            {
                Strength = 2,
                Dexterity = 12,
                Intelligence = 2
            };

            //Act
            hero.LevelUp();
            PrimaryAttributes actual = hero.BasePrimaryAttributes;

            //Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void RogueLevelUp_UpdateAttribute_AttributesPlusLevelAttributes()
        {
            //Arrange
            Rogue hero = new Rogue();
            PrimaryAttributes expected = new PrimaryAttributes
            {
                Strength = 3,
                Dexterity = 10,
                Intelligence = 2
            };

            //Act
            hero.LevelUp();
            PrimaryAttributes actual = hero.BasePrimaryAttributes;

            //Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void WarriorLevelUp_UpdateAttribute_AttributesPlusLevelAttributes()
        {
            //Arrange
            Warrior hero = new Warrior();
            PrimaryAttributes expected = new PrimaryAttributes
            {
                Strength = 8,
                Dexterity = 4,
                Intelligence = 2
            };

            //Act
            hero.LevelUp();
            PrimaryAttributes actual = hero.BasePrimaryAttributes;

            //Assert
            Assert.Equal(expected, actual);
        }
    }
}